# get_dataset

Command to untar multiple tar archives from a dataset to the local scratch on Euler using GNU parallel

Usage: get_dataset.sh [-h] [-n NUM] -d DATASET_DIR

Options:

        -h             | --help                         Display this help message and exit
        -n NUM         | --numthreads NUM               Number of threads for untaring archives
        -d DATASET_DIR | --dataset DATASET_DIR          Path to directory that contains the dataset


Example:

        get_dataset.sh -n 2 -d /cluster/work/sis/shared/datasets/test1

