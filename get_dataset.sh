#!/bin/bash

########################################################################################
# Script to unpack a data set stored in multiple tar archives on the local scratch of  #
# a compute node on Euler, using GNU parallel                                          #
#                                                                                      #
# authors: Samuel Fux, Jan-Nico Zäch                                                   #
# 2021 @ETH Zurich                                                                     #
########################################################################################

## general options: initialize variables with a default and parse command line arguments

# directory that contains the dataset provided in multiple tar archives
SOURCEDIR=$HOME

# directory (in the local scratch of a compute node) where the data set is processed during the batch job
TARGETDIR=$TMPDIR

# number of threads to be used to untar the archives
# set 1 thread as default
NUM_THREADS_TO_UNTAR=1

# function to display help message
display_help()
{
cat <<-EOF
$0: Command to untar multiple tar archives from a dataset to the local scratch on Euler using GNU parallel

Usage: get_dataset.sh [-h] [-n NUM] -d DATASET_DIR

Options:

        -h             | --help                         Display this help message and exit
        -n NUM         | --numthreads NUM               Number of threads for untaring archives
        -d DATASET_DIR | --dataset DATASET_DIR          Path to directory that contains the dataset


Example:

        get_dataset.sh -n 2 -d /cluster/work/sis/shared/datasets/test1

EOF
exit 1
}

# if no command line arguments are specified, display the help message and exit
if [[ $# -eq 0 ]]; then
        display_help
fi

# if there are some command line arguments parse them and set the variables $NUM_THREADS_TO_UNTAR and $SOURCEDIR
while [[ $# -gt 0 ]]
do
        case $1 in
                -h|--help)
                shift
                display_help
                ;;
                -n|--numthreads)
                NUM_THREADS_TO_UNTAR=$2
                shift
                shift
                ;;
                -d|--dataset)
                SOURCEDIR=$2
                shift
                shift
                ;;
                *)
                echo -e "Warning: ignoring unknown option $1 \n"
                shift
                ;;
        esac
done


## preparation steps

# check if $SOURCEDIR exists and exit this script if it does not
if ! [ -d "$SOURCEDIR" ]
    echo -e "Error: could not find $SOURCEDIR, no such directory exists, exiting script"
    exit 1
fi

# check if $TMPDIR exists (exists only inside batch job) and exit if it does not
if ! [ -d "$TMPDIR" ]
    echo -e "Error: Could not find directory \$TMPDIR (local scratch). We are not inside a batch job, exiting script"
    exit 1
fi

# create a directory on local scratch of the compute node
mkdir -p $TARGETDIR

## untaring the dataset

# there is no need to copy any data upfront as we can directly untar from the source path to the destination path
# we use GNU parallel to untar multiple files at the same time, using $NUM_THREADS_TO_UNTAR threads
# ls -1 lists one file per line, which is the format that we need for GNU parallel
echo -e "Starting to untar files from $SOURCEDIR to $TARGETDIR, using $NUM_THREADS_TO_UNTAR threads"

ls -1 $SOURCEDIR/*.tar | parallel -j $NUM_THREADS_TO_UNTAR tar xf {1} -C $TARGETDIR

echo -e "Done untaring files"
